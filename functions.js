const isArraySequential = (array) => {
  if (array.length === 0) return false;

  let sortedArray = array.slice().sort((a, b) => a - b);
  for (let i = 1; i < sortedArray.length; i++) {
    if (sortedArray[i] - sortedArray[i - 1] !== 1) {
      return false;
    }
  }
  return true;
}

const getUniqueSortedArray = (array) => {
  let uniqueElements = [...new Set(array)];
  uniqueElements.sort((a, b) => a - b);
  return uniqueElements;
}

const countOccurrences = (array) => {
  const counts = {};
  array.forEach(num => {
    counts[num] = (counts[num] || 0) + 1;
  });
  return Object.entries(counts).map(([num, count]) => ({ [num]: count }));
}

// EXAMPLE

// isArraySequential
console.log(isArraySequential([5, 2, 3, 1, 4]), 'isArraySequential example value - [5, 2, 3, 1, 4]');
console.log(isArraySequential([34, 23, 52, 12, 3]), 'isArraySequential example value - [34, 23, 52, 12, 3]');
console.log(isArraySequential([7, 6, 5, 3, 4]), 'isArraySequential example value - [7, 6, 5, 3, 4]');
// getUniqueSortedArray
console.log(getUniqueSortedArray([1, 3, 2, 2, 4, 3, 5, 6, 5]), 'getUniqueSortedArray example value - [1, 3, 2, 2, 4, 3, 5, 6, 5]');
console.log(getUniqueSortedArray([9, 9, 9, 9, 9]), 'getUniqueSortedArray example value - [9, 9, 9, 9, 9]');
console.log(getUniqueSortedArray([1, 2, 3, 4, 5]), 'getUniqueSortedArray example value - [1, 2, 3, 4, 5]');
// countOccurrences
console.log(countOccurrences([1, 3, 2, 2, 4, 3, 5, 6, 5]), 'countOccurrences example value - [1, 3, 2, 2, 4, 3, 5, 6, 5]');